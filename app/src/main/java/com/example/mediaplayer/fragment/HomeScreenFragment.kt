package com.example.mediaplayer.fragment

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.example.mediaplayer.R
import com.example.mediaplayer.databinding.HomeScreenFragmentBinding


class HomeScreenFragment : Fragment() {

    private lateinit var binding: HomeScreenFragmentBinding
    private lateinit var navController: NavController
    private val getContent = registerForActivityResult(ActivityResultContracts.GetContent()) { uri: Uri? ->
        if (uri == null) return@registerForActivityResult
        val bundle = bundleOf("uri" to uri.toString())
        navController.navigate(R.id.action_homeScreenFragment_to_videoFragment, bundle)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = HomeScreenFragmentBinding.inflate(inflater, container, false)
        navController = findNavController()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpToolBar()
        setListeners()
    }

    private fun setListeners() {
        binding.videoButton.setOnClickListener {
            getContent.launch("video/*")
        }
        binding.audioButton.setOnClickListener {
            getContent.launch("audio/*")
        }
    }

    private fun setUpToolBar() {
        (requireActivity() as AppCompatActivity).supportActionBar?.title =
            getString(R.string.app_name)
    }
}