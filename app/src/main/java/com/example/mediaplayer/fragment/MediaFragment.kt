package com.example.mediaplayer.fragment

import android.app.ActionBar.LayoutParams
import android.content.Context
import android.content.res.Configuration
import android.net.Uri
import android.os.Bundle
import android.provider.OpenableColumns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import androidx.fragment.app.Fragment
import com.example.mediaplayer.databinding.VideoFragmentBinding
import com.example.mediaplayer.fragment.MediaFragment.Constants.PROGRESS_KEY
import com.example.mediaplayer.fragment.MediaFragment.Constants.TEN_SECONDS
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MediaFragment : Fragment() {

    private lateinit var binding: VideoFragmentBinding
    private lateinit var updateSeekBar: Runnable
    private lateinit var updateTimer: Runnable
    private var progress: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = VideoFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progress = savedInstanceState?.getInt(PROGRESS_KEY) ?: 0
        setCallBacks()
        setUpVideoView()
        setUpController()
        startVideo()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(PROGRESS_KEY, timeToSeconds(binding.controller.timer.text.toString()) * 1000)
    }

    override fun onDestroy() {
        super.onDestroy()
        requireActivity().window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
    }

    private fun setCallBacks() = with(binding) {
        updateSeekBar = object : Runnable {
            override fun run() {
                controller.seekBar.progress = videoView.currentPosition
                val pos = videoView.currentPosition
                controller.seekBar.postDelayed(this, 1000L - pos % 1000)
            }
        }
        updateTimer = object : Runnable {
            override fun run() {
                adjustTimer()
                val pos = videoView.currentPosition
                controller.timer.postDelayed(this, 1000L - pos % 1000)
            }
        }
    }

    private fun removeCallBacks() = with(binding) {
        controller.seekBar.removeCallbacks(updateSeekBar)
        controller.timer.removeCallbacks(updateTimer)
    }

    private fun setUpVideoView() = with(binding) {
        if (requireActivity().resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            requireActivity().window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
            videoView.layoutParams.width = LayoutParams.MATCH_PARENT
            videoView.layoutParams.height = LayoutParams.WRAP_CONTENT
        } else {
            requireActivity().window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
            videoView.layoutParams.width = LayoutParams.WRAP_CONTENT
            videoView.layoutParams.height = LayoutParams.MATCH_PARENT
        }

        videoView.setOnClickListener {
            CoroutineScope(Dispatchers.Main).launch {
                if (controller.root.visibility == View.GONE) {
                    controller.root.visibility = View.VISIBLE
                    toolBar.visibility = View.VISIBLE
                } else {
                    controller.root.visibility = View.GONE
                    toolBar.visibility = View.GONE
                }
            }
        }

        videoView.setOnPreparedListener {
            controller.seekBar.max = videoView.duration
            controller.timer.text = stringForTime(0)
            controller.duration.text = stringForTime(videoView.duration)

            controller.seekBar.post(updateSeekBar)
            controller.timer.post(updateTimer)
        }
        videoView.setOnCompletionListener {
            controller.pauseButton.visibility = View.GONE
            controller.playButton.visibility = View.VISIBLE
        }
    }

    private fun setUpController() = with(binding) {
        controller.pauseButton.setOnClickListener {
            videoView.pause()
            controller.seekBar.removeCallbacks(updateSeekBar)
            videoView.removeCallbacks(updateTimer)
            it.visibility = View.GONE
            controller.playButton.visibility = View.VISIBLE
        }
        controller.playButton.setOnClickListener {
            videoView.start()
            controller.seekBar.post(updateSeekBar)
            videoView.post(updateTimer)
            it.visibility = View.GONE
            controller.pauseButton.visibility = View.VISIBLE
        }
        controller.forwardButton.setOnClickListener {
            videoView.seekTo(videoView.currentPosition + TEN_SECONDS)
        }
        controller.backwardButton.setOnClickListener {
            videoView.seekTo(videoView.currentPosition - TEN_SECONDS)
        }
        controller.seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                if (fromUser) {
                    videoView.seekTo(progress)
                    adjustTimer()
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
                seekBar.removeCallbacks(updateSeekBar)
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                seekBar.post(updateSeekBar)
            }
        })
    }

    private fun startVideo() = with(binding){
        val uri = Uri.parse(arguments?.getString("uri"))
        toolBar.title = uri.getName(requireContext())
        videoView.setVideoURI(uri)
        videoView.start()
        videoView.seekTo(progress)
    }

    private fun adjustTimer() = with(binding) {
        controller.timer.text = stringForTime(videoView.currentPosition)
    }

    private fun Uri.getName(context: Context): String {
        val returnCursor = context.contentResolver
            .query(this, null, null, null, null) ?: return ""
        val nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
        returnCursor.moveToFirst()
        val fileName = returnCursor.getString(nameIndex)
        returnCursor.close()
        return fileName
    }

    private fun stringForTime(timeMs: Int): String {
        val totalSeconds = timeMs / 1000
        val seconds = totalSeconds % 60
        val minutes = totalSeconds / 60 % 60

        return String.format("%02d:%02d", minutes, seconds)
    }

    private fun timeToSeconds(time: String) : Int {
        val units = time.split(":")
        val minutes = units[0].toInt()
        val seconds = units[1].toInt()
        return minutes * 60 + seconds
    }

    private object Constants {
        const val TEN_SECONDS = 10000
        const val PROGRESS_KEY = "progress_key"
    }
}